package drawing.version2;

import java.util.ArrayList;

import shapes.Circle;
import shapes.Rectangle;
import shapes.Square;

public class Drawing {

	private ArrayList<Object> shapes = new ArrayList<Object>();


	public void addShape(Object s) {
		shapes.add(s);
	}

	public double calculateTotalArea() {
		double totalArea = 0;
		for (Object s : shapes) {
			System.out.println(s.getClass());
			if (s instanceof Circle) {
				Circle c = (Circle) s;
				totalArea += c.area();
			} else if (s instanceof Rectangle) {
				Rectangle r = (Rectangle) s;
				totalArea += r.area();
			} else if (s instanceof Square) {
				Square sq = (Square) s;
				totalArea += sq.area();
			}

		}
		return totalArea;

	}
}
