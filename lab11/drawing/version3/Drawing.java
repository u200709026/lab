package drawing.version3;

import java.util.ArrayList;

import shapes.Shape;


public class Drawing {

	private ArrayList<Shape> shapes = new ArrayList<Shape>();


	public void addShape(Shape s) {
		shapes.add(s);
	}

	public double calculateTotalArea() {
		double totalArea = 0;
		for (Shape s : shapes) {
			System.out.println(s.getClass());
			totalArea += s.area();

		}
		return totalArea;

	}
}
