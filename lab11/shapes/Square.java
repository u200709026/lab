package shapes;

public class Square extends Shape {
    double side;


    public  Square(double width) {
        super();
        this.side = width;
    }

    public double area(){
        return side * side;
    }
}
