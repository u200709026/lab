package shapes;

public class Triangle extends Shape{
    double side;
    double height;


    public Triangle(double side, double height) {
        super();
        this.side = side;
        this.height = height;
    }

    public double area(){
        return (height * side) / 2;



    }

}
