package generics;


public class StackDemo {
    public static void main(String[] args) {

        Stack<Integer> stack = new StackImpl<>();
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.push(5);
        stack.push(6);

        System.out.println(stack.toList());


        Stack<Integer> stack2 = new StackImpl<>();
        stack2.push(7);
        stack2.push(8);
        stack2.push(9);

        System.out.println(stack2.toList());

        stack2.addAll(stack);
        System.out.println(stack2.toList());

        while(!stack2.empty()){
            System.out.println(stack2.pop());
        }
    }
}
