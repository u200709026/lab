package stack;

public class StackImpl implements Stack{
    StackItem top;

    @Override
    public void push(Object item) {
        StackItem elem = new StackItem(item);
        StackItem prevTop = top;
        top = elem;
        top.setNext(prevTop);
    }

    @Override
    public Object pop() {
        StackItem prevTop = top;
        top = prevTop.getNext();
        return prevTop.getItem();
    }

    @Override
    public boolean empty() {
        return top == null;
    }
}
